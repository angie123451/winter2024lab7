public class SimpleWar{
	public static void main (String[]arg){
		
		Deck deck = new Deck();
		deck.shuffle();
		System.out.println(deck);
		
		int ptangie =0;
		int ptsheila =0;
		while(deck.length()>1){
			Card angie=deck.drawTopCard();
			Card sheila=deck.drawTopCard();
			System.out.println("Angie's Card is " +angie+" and her score is " +angie.calculateScore());
			System.out.println("Sheila's Card is " +sheila+" and her score is " +sheila.calculateScore());
			
			
			if(angie.calculateScore()<sheila.calculateScore()){
				System.out.println("Sheila win the round!");
				ptsheila++;
			}else if(angie.calculateScore()>sheila.calculateScore()){
				System.out.println("Angie win the round!");
				ptangie++;
			}
				System.out.print("************************************************");
				System.out.println("");
			
		}
		if(ptangie<ptsheila){
			System.out.println("Sheila win the game with a score of "+ptsheila+"! Congrats");
		}else{
			System.out.println("Angie win the game with a score of "+ptangie+"!Congrats");
		}
		
	}
} 